# Contribuer à leximpact-dotations-ui

Avant tout, merci de votre volonté de contribuer au dépôt `leximpact-dotations-ui` !

Afin de faciliter son utilisation et d'améliorer la qualité du code, les contributions suivent certaines règles décrites dans ce fichier.

## Format des versions et du CHANGELOG

Les évolutions de `leximpact-dotations-ui` doivent pouvoir être comprises par des utilisateurs ne disposant pas du contexte complet de l'application. C'est pourquoi nous faisons le choix :

- D'un [versionnement sémantique](https://semver.org/lang/fr/) de l'application où l'évaluation des changements majeurs se fait au regard des évolutions des composants visibles pour l'usager et du format des données les alimentant.
- De l'existence d'un [CHANGELOG.md](./CHANGELOG.md), rédigé en français, pour la description des évolutions. Celui-ci se doit être le plus explicite possible.

## Mise à jour d'une version

Mettre à jour la version manuellement dans `package.json` et `CHANGELOG.md`.
Puis propager la mise à jour de la version au `package-lock.json` avec `npm install` (ou `npm install --package-lock-only` si l'on ne souhaite pas réinstaller les librairies).

## Icônes

[Remix icon](https://remixicon.com/) est favorisé. À défaut, opter pour des bibliothèques d'icônes libres.

Certaines icônes historiques proviennent de [Material UI](https://mui.com/material-ui/material-icons/).
