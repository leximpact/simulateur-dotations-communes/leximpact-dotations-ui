# LexImpact Dotations UI

## Pré-requis

Ce dépôt nécessite [NodeJS](https://nodejs.org/) v22.x.x.

Il fait appel au framework [Svelte](https://svelte.dev) en [version 5 release candidate](https://svelte-5-preview.vercel.app/docs/introduction) avec le langage [TypeScript](https://www.typescriptlang.org).

Le style est géré sur la base du framework CSS [Tailwind CSS](https://tailwindcss.com).

## Installer les dépendances

```bash
npm install
```

## Configurer l'application

`leximpact-dotations-ui` emploie trois API web :

- `territoires` pour le moteur de recherche de communes,
- `leximpact-dotations-back` pour la récupération des critères des communes et le calcul des dotations,
- `tricoteuses` pour la récupération des textes des articles de loi.

Son calcul est adaptable à une période donnée.
Son affichage est adaptable :

- à des valeurs présentées par défaut au chargement de l'application (nombre de communes, liste par défaut de communes, strates)
- à la présence et l'absence d'un Projet de loi de finances (PLF) en cours d'examen.

L'ensemble de ces éléments sont configurables dans :

- un fichier `.env` (⚠️ pour valeurs publiques uniquement) dédié en particulier à distinguer les modes intégration et production
- un fichier interne `src/lib/config.ts` pour tous les autres éléments complémentaires (liste de communes par défaut, ...).

À l'installation de l'application, vérifier la configuration du fichier `.env`.

## Démarrer l'application pour son développement

Démarrer un serveur de développement avec la commande suivante :

```bash
npm run dev

# ou démarrer le serveur et ouvrir l'application dans un nouvel onglet de navigateur avec :
npm run dev -- --open
```

L'application s'ouvre alors à l'adresse suivante : `http://localhost:5173/`

## Tester l'application

```bash
npm run test
```

Ou, pour exécuter un unique fichier de test : `npm run test src/lib/calculate.test.ts`

## Paqueter l'application

Pour créer une version de production de l'application :

```bash
npm run build
```

Il est possible de visualiser le résultat avec `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.

## Dépendance(s) à API web

Pour les calculs de dotations, `leximpact-dotations-ui` dépend de l'API web définie par [leximpact-dotations-back](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back).

> Reste à décrire : dépendance à tricotteuses pour les textes législatifs + dépendance à territoires pour la liste des territoires DGCL.
