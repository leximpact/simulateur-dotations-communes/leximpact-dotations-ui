# CHANGELOG

### 0.4.1 [!9](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-ui/-/merge_requests/9)

- Amélioration technique.
- Détails :
  - Ajoute la mise à jour du code source lors d'un déploiement en intégration
    - Complète le job de CI `deploy-integ`
    - Définit la mise à jour du service en intégration dans `.gitlab/ci/deploy.sh`

## 0.4.0 [!8](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-ui/-/merge_requests/8)

- Ajout d'une fonctionnalité. Correction d'un crash.
- Détails :
  - Corrige une erreur en CI au `deploy-integ` manuel
    - Autorise l'absence de `build/` sur le serveur d'intégration
    - Evite `rm: cannot remove 'build/*': No such file or directory` au `deploy-integ`
  - Extrait dans un `.env` les éléments de configuration usuellement distincts entre intégration et production
  - Dédie `src/lib/config.ts` aux valeurs par défaut d'initialisation de l'application
  - Ajoute les valeurs par défaut de strates à `src/lib/config.ts`

## 0.3.0 [!7](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-ui/-/merge_requests/7)

- Ajout de fonctionnalités. Améliorations techniques.
- Détails :
  - Dépendances :
    - Passe d'`adapter-auto` à `adapter-node` dans `package.json` (configuré dans `svelte.config.js`)
    - En CI, migre de `node` v`20` à v`22`.
  - Page complète : améliore les dimensions de séparation loi / impact `src/routes/+page.svelte`
  - Côté loi :
    - Ajoute les icônes des dotations devant le titre `src/lib/components/common/accordion/AccordionItem.svelte`
    - Ajoute le composant d'édition des paramètres `src/lib/components/common/LienView.svelte`
  - Côté impact :
    - Strates : ajoute les valeurs par défaut `src/lib/calculate.ts`
    - Résumé national : améliore l'alignement `src/lib/components/impact/Summary.svelte`
    - Cas types de communes :
      - requête les informations de critères de l'API web leximpact-dotations-back `src/lib/territoires.ts`
      - pour tout ajout, affiche les critères transmis par réponse d'API web `src/lib/components/impact/CardsBoard.svelte` et ajoute la description de la tendance `src/lib/components/impact/CommuneSummary.svelte`
    - Recherche de communes : améliore le contraste `src/lib/components/impact/Search.svelte`
    - Types : complète les types dans `src/lib/api.ts`, `src/lib/territoires.ts` et `src/lib/dotations.ts`
  - Configuration : initialise un fichier de configuration de l'application et de ses valeurs par défaut `src/lib/config.ts`
  - Tests : vérifie la capacité à tester un élément de page avec playwright `tests/test.ts` sur NavBar `src/routes/+layout.svelte`
  - Documentation : précise les règles de `CONTRIBUTING` pour la mise à jour de version et les icônes
  - Style : ajoute le rouge erreur et précise le périmètre des polices `tailwind.config.ts`

### 0.2.2 [!6](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-ui/-/merge_requests/6)

- Amélioration technique.
- Détails :
  - Ajoute à la CI le déploiement sur le serveur d'intégration
    - Ajoute le script de déploiement `.gitlab/ci/deploy.sh`
    - Déplace le job `release-and-tag` dans un stage dédié `publish`
    - Ajoute le job `deploy-integ` au stage `deploy`

### 0.2.1 [!3](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-ui/-/merge_requests/3)

- Amélioration technique.
- Détails :
  - Initie la configuration de la CI avec `.gitlab-ci.yml` et ses dépendances dans `.gitlab/ci/`
    - Installe et build, vérifie la version puis publie le tag de `leximpact-dotations-ui`
    - Explicite qu'on précompresse les fichiers statiques au build
  - Initie des templates pour les issues et merge requests GitLab
  - Initie une description des règles de contribution dans `CONTRIBUTING.md`

## 0.2.0 [!4](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-ui/-/merge_requests/4)

- Ajout d'une fonctionnalité. Évolution **non rétro-compatible**.
- Détails :
  - Ajoute les requêtes à `leximpact-dotations-back` via `src/lib/calculate.ts` et le typage des échanges dans `src/lib/api.ts`
  - Crée des fichiers dédiés aux objets manipulés dans `src/lib` : `territoires.ts` et `dotations.ts`
  - Passe `Svelte` des versions `^5.0.0-next.1` à `^5.1.10` (via de multiples mises à jour intermédiaires)
  - Ajoute des éléments communs `src/lib/common`
    - les composants `accordion/` et leur dépendance `strings.ts`
    - les composants `Autocomplete` du moteur de recherche (`Search`) et des paramètres éditables, `LienView` pour l'affichage d'un texte de loi par requête à https://legal.tricoteuses.fr/
  - Ajoute des éléments non communs mais qui pourraient l'être
    - `src/lib/components` : `SimulatorNavBar`
  - Ajoute des composants spécifiques à l'application
    - De la zone impact `src/lib/components/impact` : `Button`, `CardBoard`, `Card`, `CommuneSummary`, `Search`, `Table`
    - De la zone de règles `src/lib/components/rules` : `RulesAccordion`
  - Récupère les pictogrammes historiques principaux de l'ancien simulateur de dotations communales
  - Ajoute des tests unitaires auprès des composants et initie la documentation de leur usage
  - Ajoute la configuration VSCode (`.vscode/`)

## 0.1.0 [!2](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-ui/-/merge_requests/2)

- Ajout d'une fonctionnalité.
- Détails :
  - Affiche un premier texte de loi du Code général des collectivités locales
  - Récupère le texte par un appel à API web [@tricoteuses/legal-explorer](https://www.npmjs.com/package/@tricoteuses/legal-explorer)

### 0.0.2 [!1](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-ui/-/merge_requests/1)

- Amélioration technique.
- Détails :
  - Migre la dépendance de Svelte 4 à Svelte 5 (mais pas la syntaxe)
  - Ajoute la gestion du style avec TailwindCSS
  - Remplace la favicon Svelte par LexImpact
  - Initialise le README

### 0.0.1 [commit bd72e046b6b50d81e3726fd9611759e98a553f65](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-ui)

- Ajout d'une fonctionnalité.
- Détails :
  - Initialisation de l'application en Svelte 4.2.7
