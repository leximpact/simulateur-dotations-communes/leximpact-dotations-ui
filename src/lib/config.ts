import type { StrateImpact } from "$lib/api"
import type { Commune } from "$lib/territoires"
import {
  type DotationSummaryStateCommunes,
  Dotation,
  Trend,
} from "$lib/dotations"
import {
  PUBLIC_DOTATIONS_BACK_API_URL,
  PUBLIC_DOTATIONS_BACK_API_URL_ENPOINT_COMMUNE,
  PUBLIC_DOTATIONS_BACK_API_URL_ENPOINT_CALCULATE,
  PUBLIC_TERRITOIRES_API_URL,
  PUBLIC_TRICOTEUSES_API_URL,
} from "$env/static/public"

// TODO gérer la présence ou l'absence de / à la fin des URLs

export const publicApiConfigLexImpactBack = {
  url: PUBLIC_DOTATIONS_BACK_API_URL,
  endpointUrlCommune: PUBLIC_DOTATIONS_BACK_API_URL_ENPOINT_COMMUNE,
  endpointUrlCalculate: PUBLIC_DOTATIONS_BACK_API_URL_ENPOINT_CALCULATE,
}

export const publicApiConfigTerritoires = {
  url: PUBLIC_TERRITOIRES_API_URL,
}

export const publicApiConfigTricoteuses = {
  url: PUBLIC_TRICOTEUSES_API_URL,
}

export const STATE_COMMUNES_DEFAULT_DOTATION_SUMMARY: DotationSummaryStateCommunes[] =
  [
    { dotation: Dotation.DSR, nombre: 33164 },
    { dotation: Dotation.DSU, nombre: 820 },
  ]

export const COMMUNES_CAS_TYPES_DEFAULT_LIST: Commune[] = [
  {
    nomCommune: "Lillebonne",
    codeInseeCommune: "76384",
    codeInseeDepartement: "76",
    nombreHabitants: 8912,
    potentielFinancierParHabitant: 0,
    summaryDotations: [
      {
        dotation: Dotation.DF,
        eligible: true,
        montantDotation: Math.floor(Math.random() * 100000),
      },
      {
        dotation: Dotation.DSR,
        eligible: true,
        montantDotation: Math.floor(Math.random() * 10000),
      },
      {
        dotation: Dotation.DSU,
        eligible: false,
        montantDotation: Math.floor(Math.random() * 1000),
      },
    ],
  },
  {
    nomCommune: "Dénestanville",
    codeInseeCommune: "76214",
    codeInseeDepartement: "76",
    nombreHabitants: 0,
    potentielFinancierParHabitant: 0,
    summaryDotations: [
      {
        dotation: Dotation.DF,
        eligible: true,
        montantDotation: Math.floor(Math.random() * 100000),
      },
      {
        dotation: Dotation.DSR,
        eligible: true,
        montantDotation: Math.floor(Math.random() * 10000),
      },
      {
        dotation: Dotation.DSU,
        eligible: false,
        montantDotation: Math.floor(Math.random() * 1000),
      },
    ],
  },
  {
    nomCommune: "Aranc",
    codeInseeCommune: "01012",
    codeInseeDepartement: "01",
    nombreHabitants: 390,
    potentielFinancierParHabitant: 1012,
    summaryDotations: [
      {
        dotation: Dotation.DF,
        eligible: true,
        montantDotation: Math.floor(Math.random() * 100000),
      },
      {
        dotation: Dotation.DSR,
        eligible: true,
        montantDotation: Math.floor(Math.random() * 10000),
      },
      {
        dotation: Dotation.DSU,
        eligible: false,
        montantDotation: Math.floor(Math.random() * 1000),
      },
    ],
  },
]

const strate1: StrateImpact = {
  seuilHabitantsStrate: 500,
  tendance: Trend.Up,
  tauxPopulationParStrate: 0.06,
  potentielFinancierMoyenPerHabitant: 762,
  dotationsImpacts: [
    {
      dotation: Dotation.DSR,
      proportionEntitesEligibles: 0.98,
      dotationMoyenneParHabitant: 47.3,
      repartitionDotation: 0.12,
    },
    {
      dotation: Dotation.DSU,
      proportionEntitesEligibles: 0.0,
      dotationMoyenneParHabitant: 0.0,
      repartitionDotation: 0.0,
    },
  ],
}

const strate2: StrateImpact = {
  seuilHabitantsStrate: 1000,
  tendance: Trend.Up,
  tauxPopulationParStrate: 0.07,
  potentielFinancierMoyenPerHabitant: 783,
  dotationsImpacts: [
    {
      dotation: Dotation.DSR,
      proportionEntitesEligibles: 0.98,
      dotationMoyenneParHabitant: 46.38,
      repartitionDotation: 0.14,
    },
    {
      dotation: Dotation.DSU,
      proportionEntitesEligibles: 0.0,
      dotationMoyenneParHabitant: 0.0,
      repartitionDotation: 0.0,
    },
  ],
}

const strate3: StrateImpact = {
  seuilHabitantsStrate: 2000,
  tendance: Trend.Up,
  tauxPopulationParStrate: 0.09,
  potentielFinancierMoyenPerHabitant: 852,
  dotationsImpacts: [
    {
      dotation: Dotation.DSR,
      proportionEntitesEligibles: 0.98,
      dotationMoyenneParHabitant: 48.97,
      repartitionDotation: 0.2,
    },
    {
      dotation: Dotation.DSU,
      proportionEntitesEligibles: 0.0,
      dotationMoyenneParHabitant: 0.0,
      repartitionDotation: 0.0,
    },
  ],
}

const strate4: StrateImpact = {
  seuilHabitantsStrate: 3500,
  tendance: Trend.Up,
  tauxPopulationParStrate: 0.09,
  potentielFinancierMoyenPerHabitant: 926,
  dotationsImpacts: [
    {
      dotation: Dotation.DSR,
      proportionEntitesEligibles: 0.97,
      dotationMoyenneParHabitant: 49.08,
      repartitionDotation: 0.19,
    },
    {
      dotation: Dotation.DSU,
      proportionEntitesEligibles: 0.0,
      dotationMoyenneParHabitant: 0.0,
      repartitionDotation: 0.0,
    },
  ],
}

const strate5: StrateImpact = {
  seuilHabitantsStrate: 5000,
  tendance: Trend.Up,
  tauxPopulationParStrate: 0.06,
  potentielFinancierMoyenPerHabitant: 1015,
  dotationsImpacts: [
    {
      dotation: Dotation.DSR,
      proportionEntitesEligibles: 0.95,
      dotationMoyenneParHabitant: 48.32,
      repartitionDotation: 0.13,
    },
    {
      dotation: Dotation.DSU,
      proportionEntitesEligibles: 0.0,
      dotationMoyenneParHabitant: 0.0,
      repartitionDotation: 0.0,
    },
  ],
}

const strate6: StrateImpact = {
  seuilHabitantsStrate: 7500,
  tendance: Trend.Up,
  tauxPopulationParStrate: 0.07,
  potentielFinancierMoyenPerHabitant: 1040,
  dotationsImpacts: [
    {
      dotation: Dotation.DSR,
      proportionEntitesEligibles: 0.93,
      dotationMoyenneParHabitant: 42.73,
      repartitionDotation: 0.13,
    },
    {
      dotation: Dotation.DSU,
      proportionEntitesEligibles: 0.0,
      dotationMoyenneParHabitant: 0.0,
      repartitionDotation: 0.0,
    },
  ],
}

const strate7: StrateImpact = {
  seuilHabitantsStrate: 10000,
  tendance: Trend.Down,
  tauxPopulationParStrate: 0.05,
  potentielFinancierMoyenPerHabitant: 1090,
  dotationsImpacts: [
    {
      dotation: Dotation.DSR,
      proportionEntitesEligibles: 0.85,
      dotationMoyenneParHabitant: 32.71,
      repartitionDotation: 0.07,
    },
    {
      dotation: Dotation.DSU,
      proportionEntitesEligibles: 0.0,
      dotationMoyenneParHabitant: 0.0,
      repartitionDotation: 0.0,
    },
  ],
}

const strate8: StrateImpact = {
  seuilHabitantsStrate: 15000,
  tendance: Trend.Down,
  tauxPopulationParStrate: 0.07,
  potentielFinancierMoyenPerHabitant: 1123,
  dotationsImpacts: [
    {
      dotation: Dotation.DSR,
      proportionEntitesEligibles: 0.08,
      dotationMoyenneParHabitant: 2.69,
      repartitionDotation: 0.01,
    },
    {
      dotation: Dotation.DSU,
      proportionEntitesEligibles: 0.0,
      dotationMoyenneParHabitant: 0.0,
      repartitionDotation: 0.0,
    },
  ],
}

const strate15: StrateImpact = {
  seuilHabitantsStrate: Infinity,
  tendance: Trend.Down,
  tauxPopulationParStrate: 0.09,
  potentielFinancierMoyenPerHabitant: 1627,
  dotationsImpacts: [
    {
      dotation: Dotation.DSR,
      proportionEntitesEligibles: 0,
      dotationMoyenneParHabitant: 0,
      repartitionDotation: 0,
    },
    {
      dotation: Dotation.DSU,
      proportionEntitesEligibles: 0.0,
      dotationMoyenneParHabitant: 0.0,
      repartitionDotation: 0.0,
    },
  ],
}

export const STRATES_COMMUNES_DEFAULT_LIST: StrateImpact[] = [
  strate1,
  strate2,
  strate3,
  strate4,
  strate5,
  strate6,
  strate7,
  strate8,
  strate15,
]
