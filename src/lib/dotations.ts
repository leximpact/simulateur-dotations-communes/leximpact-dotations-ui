export enum Dotation {
  DF = "DF",
  DSR = "DSR",
  DSU = "DSU",
  // TODO ajouter la dotation commune nouvelle
}

export enum Trend {
  Up,
  Down,
  Stable,
}

export type DotationSummaryCommune = {
  dotation: string
  eligible: boolean
  montantDotation: number
}

export type DotationSummaryStateCommunes = {
  dotation: Dotation
  nombre: number
}

export function getDotationIconName(dotation: Dotation) {
  // uses Material-UI icons (google)
  // TODO fix icons ; check https://iconify.design/docs/usage/css/tailwind/
  if (dotation === Dotation.DF) {
    return "mdi:briefcase-variant"
  } else if (dotation === Dotation.DSR) {
    return "mdi:flower"
  } else if (dotation === Dotation.DSU) {
    return "mdi:city"
  } else {
    // TODO error on undefined?
    return ""
  }
}

export function getTrendIconPath(trend: Trend) {
  if (trend === Trend.Up) {
    return "./picto-dotation-tendance-monte.svg"
  } else if (trend === Trend.Down) {
    return "./picto-dotation-tendance-baisse.svg"
  } else {
    // Trend.Stable or undefined
    // TODO error on undefined?
    return ""
  }
}
