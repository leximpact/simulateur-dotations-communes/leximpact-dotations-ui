import { beforeAll, describe, expect, test, vi } from "vitest"
import { DOTATIONS_BACK_API_URL, calculateDotations } from "./calculate"
import type { ApiCalculateRequest, ApiCalculateResponse } from "./api"

const BEFORE_ALL_TIMEOUT = 30000 // 30 sec

describe("appel minimal à calculateDotations", () => {
  let payload: ApiCalculateRequest = {
    base: {
      dotations: {
        dotation_globale_fonctionnement: {
          enveloppe: 42,
        },
      },
      casTypes: [],
      strates: [],
    },
  }

  let fetchSpy, response: ApiCalculateResponse
  beforeAll(async () => {
    fetchSpy = vi.spyOn(globalThis, "fetch")
    response = await calculateDotations(payload) //TODO try error not valid json ?

    expect(fetchSpy).toHaveBeenCalledWith(
      DOTATIONS_BACK_API_URL + "/calculate",
      expect.anything(),
    )
  }, BEFORE_ALL_TIMEOUT)

  test("vérifie le contenu minimal d'une réponse sans erreur", async () => {
    const expected: ApiCalculateResponse = {
      base: {
        dotations: {
          dotation_globale_fonctionnement:
            payload.base.dotations.dotation_globale_fonctionnement,
          dotation_forfaitaire: null, // None Python devient null TypeScript
          dotation_solidarite_rurale: null,
          dotation_solidarite_urbaine: null,
        },
        casTypes: [],
        strates: [],
      },
      amendement: null,
      plf: null,

      baseToAmendement: null,
      baseToPlf: null,
    }
    expect(response).toEqual(expected)
  })
})
