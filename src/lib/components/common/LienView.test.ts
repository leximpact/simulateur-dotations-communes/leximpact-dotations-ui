import { render, screen } from "@testing-library/svelte"
import LienView from "./LienView.svelte"
import viewLegifranceArticle from "./LienView.svelte"

describe("Composant LienView", () => {
  test("Sample test", () => {
    expect(1 + 3).equal(4)
  })

  test("view legifrance article", async () => {
    let componentOptions = {}
    // let renderOptions = {}  // svelte5 info: https://testing-library.com/docs/svelte-testing-library/api#baseelement
    // const { baseElement, container } =
    render(LienView, componentOptions)
    // expect(baseElement).toBe(document.body)

    // article L2334-13 du CGCT en vigueur depuis le 31/12/2023
    const articleText = await viewLegifranceArticle({}, {})
    expect(articleText).toBe("290 millions d'euros en 2024")
    console.log(articleText)
  })
})
