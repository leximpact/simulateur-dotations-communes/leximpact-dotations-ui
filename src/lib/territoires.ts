import type { ApiCommuneRequest, ApiCommuneResponse } from "$lib/api"
import type { DotationSummaryCommune } from "$lib/dotations"
import { publicApiConfigLexImpactBack } from "$lib/config"

const DOTATIONS_BACK_API_URL_ENPOINT_COMMUNE =
  publicApiConfigLexImpactBack.endpointUrlCommune

export type Commune = {
  nomCommune: string
  codeInseeCommune: string // string pour code débutant par zéro
  codeInseeDepartement: string // string pour code débutant par zéro

  nombreHabitants?: number
  potentielFinancierParHabitant?: number
  summaryDotations?: DotationSummaryCommune[]
}

export async function collectCommuneInformation(
  request: ApiCommuneRequest,
): Promise<ApiCommuneResponse> {
  console.debug("collectCommuneInformation...")
  console.debug(request)

  let requestSettings = {
    // mode: "no-cors", // TODO corriger le mode
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json", //TODO ; charset=utf-8
    },
    body: JSON.stringify(request),
  }

  let result = undefined
  try {
    let errorMessage = ""
    let response = await fetch(
      DOTATIONS_BACK_API_URL_ENPOINT_COMMUNE,
      requestSettings,
    )

    console.debug("▶️ API dotations back response: ")
    console.debug(response)

    if (!response.ok) {
      errorMessage =
        "Erreur à la recheche d'information sur la commune '" +
        request.nomCommune +
        "' (ERREUR " +
        response.status + // TODO debug zero
        ")."
      if (response.status === 422) {
        // 422 ABORTED Unprocessable Entity
        errorMessage +=
          " Requête envoyée à l'API web incorrectes (ERREUR 422, " +
          DOTATIONS_BACK_API_URL_ENPOINT_COMMUNE +
          ")."
      } else if (response.status === 500) {
        errorMessage +=
          " API web injoignable : " + DOTATIONS_BACK_API_URL_ENPOINT_COMMUNE
      }
      result = {
        ...request,
        error: errorMessage,
      }
    } else {
      result = await response.json()
    }

    console.debug(errorMessage)
  } catch (error) {
    console.debug("✈️")
    console.error(error)
  }

  console.debug("collectCommuneInformation response:")
  console.debug(result)
  return result
}
