import type {
  ApiCalculateRequest,
  ApiCalculateResponse,
  StrateImpact,
} from "$lib/api"
import {
  publicApiConfigLexImpactBack,
  STRATES_COMMUNES_DEFAULT_LIST,
} from "$lib/config"

const DOTATIONS_BACK_API_URL_ENPOINT_CALCULATE =
  publicApiConfigLexImpactBack.endpointUrlCalculate

export function getStrates(): StrateImpact[] {
  return STRATES_COMMUNES_DEFAULT_LIST
}

export async function calculateDotations(
  request: ApiCalculateRequest,
): Promise<ApiCalculateResponse> {
  console.debug("calculateDotations...")
  console.debug(request)

  const requestSettings = {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(request),
  }

  const response = await fetch(
    DOTATIONS_BACK_API_URL_ENPOINT_CALCULATE,
    requestSettings,
  )

  let result
  if (!response.ok) {
    result = { error: response.status }
  } else {
    result = await response.json()
  }

  return result
}
