// types
import type { Commune } from "$lib/territoires"
import type { Dotation, Trend } from "$lib/dotations"

export type ApiCommuneRequest = Commune // identification principale via Commune.codeInseeCommune
export type ApiCommuneResponse = Commune & { error?: string } // Commune où tous les champs optionnels devraient être remplis

type DotationImpact = {
  dotation: Dotation // nom (acronyme)
  proportionEntitesEligibles: number // pourcentage
  dotationMoyenneParHabitant: number // euros
  repartitionDotation: number // pourcentage
}

export type StrateImpact = {
  seuilHabitantsStrate: number // nombre d'habitants
  tendance: Trend
  tauxPopulationParStrate: number // pourcentage
  potentielFinancierMoyenPerHabitant: number //euros
  dotationsImpacts: DotationImpact[]
}

type DotationsCommunes = {
  dotation_globale_fonctionnement?: any
  dotation_forfaitaire?: any
  dotation_solidarite_rurale?: any
  dotation_solidarite_urbaine?: any
}

type UiThemes = {
  dotations: DotationsCommunes
  casTypes: []
  strates: []
}

export type ApiCalculateRequest = {
  base: UiThemes
  amendement?: UiThemes
  plf?: UiThemes
}

export type ApiCalculateResponse =
  | {
      base: UiThemes
      amendement?: UiThemes | null
      plf?: UiThemes | null

      baseToAmendement?: UiThemes | null
      baseToPlf?: UiThemes | null
    }
  | {
      error: string
    }
