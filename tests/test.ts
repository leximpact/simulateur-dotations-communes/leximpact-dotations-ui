import { expect, test } from "@playwright/test"

test("index page has expected h1", async ({ page }) => {
  await page.goto("/")

  const navBarElement = await page.locator("#nav-bar")
  const navBarDiv = await navBarElement.locator("div")

  // TODO const spanTitle =
  await navBarDiv.locator("div > div:nth-child(2) span")
  // TODO const title = await spanTitle.textContent()
  // TODO expect(title).toContain("LexImpact")
})
