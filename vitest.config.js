import { defineConfig } from "vitest/config"
import { svelte } from "@sveltejs/vite-plugin-svelte"

export default defineConfig({
  plugins: [svelte({ hot: !process.env.VITEST })],
  test: {
    alias: {
      "@testing-library/svelte": "@testing-library/svelte/svelte5",
    },
    environment: "jsdom",
    globals: true, // to avoid "import { describe, expect, test } from 'vitest'" in every test file
    setupFiles: ["./vitest-setup.js"],
  },
})
