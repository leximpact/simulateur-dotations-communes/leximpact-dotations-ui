#!/bin/bash

# USAGE: to be copied by the CI to the integration or production container and executed there

# stop at first error
set -e

# INFO:
# no need to stop the service or delete the previous build before (re)build
# (in case of memory shortage move the building step to the CI)
echo "Update the application's dependencies and build..."
npm install
npm run build

echo "Restart the service after build update..."
systemctl --user restart dotations-ui-integ.service

echo "Deployment done! 🎉"
