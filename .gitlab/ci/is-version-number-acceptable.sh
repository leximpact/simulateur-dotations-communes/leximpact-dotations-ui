#! /usr/bin/env bash

if [[ $CI_COMMIT_BRANCH == main ]]
then
    echo "No need for a version check on main."
    exit 0
fi

if ! $(dirname "$BASH_SOURCE")/has-functional-changes.sh
then
    echo "No need for a version update."
    exit 0
fi

# to retrieve the version, use same command here and in latest release/tag naming
current_version=`cat package.json | grep \"version\" | cut -d'"' -f 4`

if git rev-parse --verify --quiet $current_version
then
    echo "Version $current_version already exists in commit:"
    git --no-pager log -1 $current_version
    echo
    echo "Update the version number in package.json before merging this branch into main."
    echo "Look at the CONTRIBUTING.md file to learn how the version number should be updated."
    exit 1
fi

if ! $(dirname "$BASH_SOURCE")/has-functional-changes.sh | grep --quiet CHANGELOG.md
then
    echo "CHANGELOG.md has not been modified, while functional changes were made."
    echo "Explain what you changed before merging this branch into main."
    echo "Look at the CONTRIBUTING.md file to learn how to write the changelog."
    exit 2
fi
