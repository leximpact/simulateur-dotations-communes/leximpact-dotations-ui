Merci de contribuer à leximpact-dotations-ui ! Effacez cette ligne ainsi que, pour chaque ligne ci-dessous, les cas ne correspondant pas à votre contribution :)

* Ajout d'une fonctionnalité. | Évolution **non rétro-compatible**. | Amélioration technique. | Correction d'un crash. | Changement mineur.
* Détails :
  - Description de la fonctionnalité ajoutée ou du nouveau comportement adopté.
  - Cas dans lesquels une erreur était constatée.
  - Eventuel guide de migration pour les réutilisations.
